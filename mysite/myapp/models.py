

# Create your models here.
from django.db import models
# from mailprocess.models import ModelBase
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
# from template_manager.models import EmailTemplate


# Create your models here.
class Project(models.Model):
    title = models.CharField(max_length=100)
    status = models.TextField(null=True)
    task_status = models.JSONField(null=True)
    task_priority = models.JSONField(null=True)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="project_owner")
    group = models.CharField(max_length=100, null=True, blank=True)
    task_layout = models.CharField(max_length=100, null=True, blank=True)
    tags = models.CharField(max_length=100, null=True, blank=True)
    # customize_tab = models.TextField(null=True, blank=True)
    access_project = models.BooleanField(default=False)
    # is_strict = models.BooleanField(default=False)
    project_overview = models.TextField(null=True)
    # template = models.ForeignKey(EmailTemplate, on_delete=models.CASCADE, null=True)
    slug = models.SlugField(unique=True, max_length=254)

    def save(self, *args, **kwargs):
        if not self.id:
            super(Project, self).save(*args, **kwargs)
        self.slug = slugify(self.title) + "-" + str(self.user.id) + "-" + str(self.id)
        super(Project, self).save(*args, **kwargs)

    def _str_(self):
        return str(self.title)
