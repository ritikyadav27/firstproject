from django.shortcuts import render
from myapp.models import Project

# Create your views here.
def index(request):
    return render(request, "index.html")